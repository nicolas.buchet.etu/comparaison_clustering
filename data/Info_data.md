__data__: contient les données brutes et traitées.

- __all_pair.rds__    ->   contient les sextuplets de paires générés par ../script/arrangement.R mais tronqué. Il contient donc l'ensemble des sextuplets de paires possibles pour deux méthodes donnant 6 clusters.
- __clusters.rds__    ->   après avoir générer les clusters pour chaque méthodes, les (6*4) 24 clusters obtenus sont stockés dans cette listes pour être réutilisés ailleurs
- __rma_merge__       ->   jeu de données normalisées non filtrés
		

__Lien utiles__: 

- __Article__ : https://www.nature.com/articles/srep30013
- __Direct link to data__ : https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE74453
- __Configuration file of the chip__ : https://www.thermofisher.com/order/catalog/product/901085?SID=srch-srp-901085

