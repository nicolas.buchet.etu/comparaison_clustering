f = open("Hugene_4", 'r')
lines = f.readlines()

probe_id = []
genes = []

total = len(lines)
pluricompte = 0

for line in lines:
    line = line.strip()
    bichamps = line.split("\t")
    probe_id.append(bichamps[0])
    
    names1 = bichamps[1]
    names1 = names1.split(' /// ')
    names2 = []
    for truc in names1:
        truc = truc.split(' // ')
        names2.append(truc[1])
    
    first = names2[0]
    pluri = False
    for nom in names2:
        if first != nom:
            pluri = True
    if pluri:
        pluricompte+=1

    genes.append(first)
    
for i in range(total):
    print(probe_id[i] + '\t' + genes[i])
