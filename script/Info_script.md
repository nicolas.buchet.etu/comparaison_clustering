__script__: contient les scripts des différents méthodes

- __Python__    ->  dossier contenant les scripts python permettant de créer le fichier d'annotation des données
- __ACP.R__ ->  script R permettant de réaliser l'ACP a partir des données normalisées.
- __Unused.Rmd__    ->  regroupement de divers fonctions explorer au cour du projet mais non utilisé.
- __arrangement.R__ ->  script R permettant d'arranger les clusters des différentes méthodes entre eux.
- __comparaison.Rmd__   ->  script R permettant la comparaison des méthodes de clustering.

Le script utilisé pour l'enrichissement des clusters a était perdu (script R utilisant la fonction enrichDAVID du package "clusterProfiler"). Remarque: ce script était présent sur un Rserveur de la plateforme bilille créer spécialement pour le projet et possédant les dépendances nécessaire a l'utilisation de cette fonction.	
