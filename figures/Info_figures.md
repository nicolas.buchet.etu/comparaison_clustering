__figures__: contient les figures obtenues grâce au script comparaison.Rmd avec le jeu de données filtré pour un écart-type > 1.5
  
__Graphiques réalisées avec la fonction _fviz_cluster_ dans le but de pouvoir comparer efficacement visuellement les différentes méthodes__ :
- __agnes.png__    ->   graphique du clustering hiérarchique avec la fonction _agnes_ 
- __kmeans.png__  ->   graphique du clustering par la méthode k-means avec la fonction _kmeans_
- __clara.png__    ->   graphique du clustering par la méthode PAM avec la fonction _clara_
- __em.png__    ->   graphique du clustering par la méthode EM avec la fonction _Mclust_ 

__Autres figures__ :
- __clusters_comparaison.png__  ->  graphique montrant les clusters qui sont les plus similaires entre les méthodes.
- __dendo_agnes.png__   ->  dendrogramme réalisé par clustering hiérarchique sur les gènes avec la fonction _agnes_
- __dendo_cells.png__   ->  dendrogramme réalisé par clustering hiérarchique sur les individus avec la fonction _agnes_
- __hauteur_des_branches.png__  ->  graphique de la hauteur des branches qui permet de déterminer un nombre de cluster correct pour réaliser les différentes méthodes de clustering (dans notre cas : **6 clusters**)

Le dossier **ACP** contient les différents graphiques réalisés à partir d'une Analyse en Composante Principale (ACP) qui a permis de comprendre que les grapĥiques obtenus avec la fonction _fviz_cluster_ réalise une ACP.
