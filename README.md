# Comparaison d’approches de clustering sur des données de transcriptomique

## Sommaire

1. [Description](#description)
2. [Technologies](#technologies)
3. [Installation](#installation)
4. [Contenu du git](#contenu-du-git)
5. [Fonctionnement du code](#fonctionnement-du-code)
6. [Limite du code](#limite-du-code)
7. [Collaboration](#collaboration)

## Description

**Les techniques de clustering sont couramment utilisées dans le cadre d’un apprentissage dit non-supervisé (sans connaissance a priori sur la classe des données traitées).** 
Appliquées à des **données de transcriptomiques**, ces approches permettent d'identifier des groupes de gènes corrélés dû à un phénomène de co-régulation ou d'association à une maladie ou à des conditions expérimentales testées dans le cadre d'une étude. 
De nombreuses méthodes de **clustering** existent, parmi lesquelles le **clustering hiérarchique**, les **K-means** ou encore le clustering basé sur des **modèles de mélange**, approches qui ont été largement utilisées en bioanalyse comme pour d'autres types de données.

Le projet vise à **comparer différentes approches de clustering sur des données de transcriptomique** disponibles dans la littérature. 
En fonction des jeux de données retenus, un pré-traitement pourra être appliqué afin d'obtenir des données d'expression de gènes sur lesquelles appliquer de 3 à 4 approches différentes de clustering. 
Les classes identifiées par ces différentes approches seront tout d'abord comparées entre elles afin d'évaluer la concordance des approches. 
Dans un second temps, afin d'évaluer la pertinence biologique des clusters identifiés par chaque approche, une comparaison sur la base des ontologies de gènes disponibles dans les bases de référence sera effectuée.

## Technologies

Liste des technologies utilisées :

* [Galaxy](http://193.54.101.167/): Prétraitement des données  
* [R](https://www.rstudio.com/products/rstudio/download/): Version 4.1.2
* [RStudio](https://www.rstudio.com/products/rstudio/download/):  Version 1.4.1106
* [Python](https://www.python.org/downloads/) : Version 3.8.10

## Installation

Pour utiliser le code, vous aurez besoin de cloner le dépot.

```
$ git clone https://gitlab.univ-lille.fr/nicolas.buchet.etu/comparaison_clustering.git
```

## Contenu du git

__Dossiers__:
- __data__: contient les données brutes et traitées.
- __figures__: contient les figures obtenues grace aux scripts.
- __script__: contient les scripts des différents méthodes.

__Fichiers__:
- __README.md__ : ce document expliquant brièvement le fonctionnement du code ainsi que l'architecture du git.
- __comparaison.html__ : fichier issus de `./script/comparaison.Rmd` montrant l'intégralité du code lancés avec les résultats
- __report\_Souc\_Gerenton\_Buchet.pdf__ : le rapport en anglais du projet réalisé, avec toutes les informations sur le déroulement du projet et les résultats obtenues

Chaque dossier contient un fichier `Info_<nom_du_dossier>.md` avec le détail du fichier du dossier.

## Fonctionnement du code

Le code principal est `comparaison.Rmd`, donc les explications ci-dessous d'appliquent (sauf précision) à ce document.

### Méthodes étudiées individuellement

4 méthodes de clustering différentes sont utilisées en gardant 6 clusters.

- La méthode des **k-means** ( fonction `kmeans` de `stats` )
- La méthode de **classification hiérarchique** ( fonction `agnes` de `cluster` )
- La méthode **clara** ( fonction `clara` de `cluster` )
- La méthode **EM** ( fonction `Mclust` de `mclust` )

À noter que Mclust réalise en réalité plusieurs clustering en changeant ses paramètres pour nous donner uniquement la meilleure des méthodes.

Puis, des figures sont générés pour représentés les résultats de ces méthodes et sont retrouvables dans `./figures`.

### Méthodes comparées deux à deux

Avec `table`, il est affiché le nombre de gène en commun entre chacun des clusters renvoyés par les méthodes.
Or, ces données ne peuvent pas être directement utilisés car elles ne prenent pas en compte la taille des cluster.

Pour cela, la fonction `en_pourcentage` construite pour l'occasion permet de donner un "pourcentage d'identité" entre cluster définit comme suit :

> C'est le nombre de gène présent dans les deux clusters comparés par rapport au nombre de gène présent dans au moins un des deux clusters.

Deux approches sont proposées pour, avec 2 méthodes, rassembler leurs cluster les plus proches.
- Maximiser la moyenne du pourcentage d'identité pour chaque combinaison possible (`midsim`)
- Maximiser le nombre de gène en commun dans chaque pair de cluster pour chaque combinaison possible (`totexpl`)

La première approche sera utilisée dans la suite du code.

L'objet `all_pair` est ensuite chargé. Il a été générés à côté grâce à `./script/arrangement.R`. Il contient l'ensemble des sextuplets de paires possibles entre les 6 clusters de 2 méthodes différentes.

`best_combi` va trouver la paire maximisant le score de similarité.

Puis en Network Plot est réalisé. Chaque cluster d'une méthode est relié au 3 autres clusters les plus proches dans chacune des 3 autres méthodes. Il est visible dans `./figures`. Il nous permet de voir les rapprochements entre les méthodes d'une autre façon que des tableaux de similarité.

### Enrichissement

L'enrichissement des clusters a été fait sur un RServer mis en place par la plateforme bilille.
Le script utilisé n'a pas été récupéré et est donc perdu.

# Limite du code

Aucune comparaison optimal des 4 méthodes en même temps n'est réalisé. Une réalisation de cette comparaison ainsi que la construction d'un diagramme de Venn montrant les similitudes entres les clusters proches des 4 méthodes concluraient mieux le projet. 

## Collaboration
### Project Manager
Jimmy Vandel – jimmy.vandel@univ-lille.fr, bilille

### Auteurs
Faustine SOUC \
Pierre GERENTON \
Nicolas BUCHET
